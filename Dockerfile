FROM node:20 AS builder

COPY package.json \
     package-lock.json \
     /usr/web/

WORKDIR /usr/web/
RUN npm install

COPY src/ /usr/web/src/
RUN npm run build

FROM rtsp/lighttpd
COPY --from=builder /usr/web/dist/ /var/www/html/